package ru.jokebot.integration.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import ru.jokebot.integration.model.entity.User;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class UserRepository {

    private final UserMapper userMapper;

    public Optional<User> getUserById(Long id) {
        User user = userMapper.getUserById(id);
        return Optional.ofNullable(user);
    }

    public User save(Long id, String locale) {
        User user = new User(id, locale);
        userMapper.saveUser(user);
        return user;
    }

    public void edit(User user) {
        userMapper.editUser(user);
    }
}
