package ru.jokebot.integration.repository;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import ru.jokebot.integration.model.entity.User;

@Mapper
public interface UserMapper {

    @Select("SELECT id, locale FROM user_data WHERE id = #{id}")
    User getUserById(Long id);

    @Insert("INSERT INTO user_data (id, locale) VALUES (#{id}, #{locale})")
    void saveUser(User user);

    @Update("UPDATE user_data SET locale = #{locale} WHERE id = #{id}")
    void editUser(User user);
}
