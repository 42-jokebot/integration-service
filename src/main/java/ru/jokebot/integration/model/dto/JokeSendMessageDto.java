package ru.jokebot.integration.model.dto;

public record JokeSendMessageDto(Long userId,
                                 Long jokeId) {

}
