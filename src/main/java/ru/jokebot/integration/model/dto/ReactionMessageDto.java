package ru.jokebot.integration.model.dto;

public record ReactionMessageDto(Long userId,
                                Integer reactionId,
                                Long jokeId) {

}
