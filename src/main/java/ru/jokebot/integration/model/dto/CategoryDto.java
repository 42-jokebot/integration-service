package ru.jokebot.integration.model.dto;

public record CategoryDto(Long id,
                          String categoryName) {

}
