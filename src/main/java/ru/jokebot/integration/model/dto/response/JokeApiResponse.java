package ru.jokebot.integration.model.dto.response;

import java.util.HashMap;

public record JokeApiResponse(Boolean error,
                              String category,
                              String type,
                              String joke,
                              String setup,
                              String delivery,
                              HashMap<String, Boolean> flags,
                              Long id,
                              Boolean safe,
                              String lang) {

}
