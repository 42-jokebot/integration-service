package ru.jokebot.integration.model.dto.response;

public record CategoryGetResponse(Long id,
                                  String categoryName,
                                  String locale) {
}
