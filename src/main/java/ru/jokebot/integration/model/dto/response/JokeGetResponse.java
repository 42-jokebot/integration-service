package ru.jokebot.integration.model.dto.response;

public record JokeGetResponse(Long id,
                              String joke) {

}
