package ru.jokebot.integration.model.dto;

public record ExternalResourceJokeDto(String jokeText,
                                      String category,
                                      String locale) {

}
