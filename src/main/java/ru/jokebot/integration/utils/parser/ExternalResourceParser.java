package ru.jokebot.integration.utils.parser;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.jokebot.integration.exception.parser.JokeNotFoundException;
import ru.jokebot.integration.exception.parser.ParsingException;
import ru.jokebot.integration.model.dto.ExternalResourceJokeDto;
import ru.jokebot.integration.utils.parser.strategy.AnekdotiRuParsingStrategy;
import ru.jokebot.integration.utils.parser.strategy.AnekdotiRuTaggedParsingStrategy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

@Component
@Slf4j
public class ExternalResourceParser {

    private final static HashMap<String, ParseResource> russianResourcesForParsing = new HashMap<>();
    private final static HashMap<String, ParseResource> englishResourcesForParsing = new HashMap<>();

    static {
        russianResourcesForParsing.put("anekdoti.ru", new ParseResource("https://www.anekdot.ru/random/anekdot", new AnekdotiRuParsingStrategy()));
        russianResourcesForParsing.put("taggedAnekdoti.ru", new ParseResource("https://www.anekdot.ru/tags/", new AnekdotiRuTaggedParsingStrategy()));
    }

    public ExternalResourceJokeDto getJoke(String category, String locale) {
        if (locale.equals("ru")) {
            return getJokeFromResource(category, russianResourcesForParsing);
        } else {
            return getJokeFromResource(category, englishResourcesForParsing);
        }
    }

    private static ExternalResourceJokeDto getJokeFromResource(String category, HashMap<String, ParseResource> resourceMap) {
        Random random = new Random();
        ArrayList<String> resourcesPool = new ArrayList<>(resourceMap.keySet().stream().toList());

        while (!resourcesPool.isEmpty()) {
            String resource = resourcesPool.get(random.nextInt(resourcesPool.size()));
            try {
                ParseResource parseResource = resourceMap.get(resource);
                if (parseResource.strategy().getCategories().contains(category.toLowerCase())) {
                    return resourceMap.get(resource).parseJoke(category);
                }
            } catch (ParsingException ex) {
                log.warn("Failed to parse joke from resource: {}, exception: {}", resource, ex.getMessage());
                log.warn("Trying to parse joke from another resource...");
            } finally {
                resourcesPool.remove(resource);
            }
        }
        throw new JokeNotFoundException("Couldn't find joke on external resources for this category");
    }
}
