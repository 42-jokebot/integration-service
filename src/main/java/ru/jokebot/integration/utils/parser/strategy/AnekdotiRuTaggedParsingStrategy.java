package ru.jokebot.integration.utils.parser.strategy;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import ru.jokebot.integration.exception.parser.ParsingException;
import ru.jokebot.integration.model.dto.ExternalResourceJokeDto;
import ru.jokebot.integration.utils.parser.AnekdotiRuParsingUtils;
import ru.jokebot.integration.utils.parser.ParsingStrategy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class AnekdotiRuTaggedParsingStrategy implements ParsingStrategy {

    private final Map<String, String> siteCategories = new HashMap<>();

    public AnekdotiRuTaggedParsingStrategy() {
        siteCategories.put("вовочка", "вовочка/%d?type=anekdots");
        siteCategories.put("программист", "программист/%d?type=anekdots");
        siteCategories.put("новогодний", "новогодний/%d?type=anekdots");
        siteCategories.put("политика", "политика/%d?type=anekdots");
        siteCategories.put("автомобили", "авто/%d?type=anekdots");
    }

    @Override
    public ExternalResourceJokeDto parse(String siteUrl, String category) {
        Random random = new Random();
        String finalUrl = siteUrl + String.format(siteCategories.get(category.toLowerCase()), random.nextInt(10));

        Document document;
        try {
            document = Jsoup.connect(finalUrl).get();
            return new ExternalResourceJokeDto(AnekdotiRuParsingUtils.parseJokeText(document), category, "ru");
        } catch (Exception e) {
            throw new ParsingException("Exception while parsing " + finalUrl + ": " + e.getMessage());
        }
    }

    @Override
    public List<String> getCategories() {
        return siteCategories.keySet().stream().toList();
    }
}
