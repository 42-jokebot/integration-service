package ru.jokebot.integration.utils.parser;

import lombok.experimental.UtilityClass;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import ru.jokebot.integration.exception.parser.ParsingException;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@UtilityClass
public class AnekdotiRuParsingUtils {

    public static String parseJokeText(Document document) {
        Random random = new Random();
        try {
            List<Integer> dataIds = new ArrayList<>();
            for (Element element : document.getElementsByAttribute("data-id")) {
                dataIds.add(Integer.parseInt(element.attr("data-id")));
            }

            int randomDataId = dataIds.get(random.nextInt(dataIds.size()));
            Element jokeElement = document.getElementsByAttributeValue("data-id", String.valueOf(randomDataId)).first();

            return jokeElement.getElementsByClass("text")
                    .first().wholeOwnText().replace("<br>", "\n");
        } catch (NullPointerException e) {
            throw new ParsingException("Exception while parsing random joke: " + e.getMessage());
        }
    }
}
