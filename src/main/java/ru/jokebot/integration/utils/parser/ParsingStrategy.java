package ru.jokebot.integration.utils.parser;

import ru.jokebot.integration.model.dto.ExternalResourceJokeDto;

import java.util.List;

public interface ParsingStrategy {

    ExternalResourceJokeDto parse(String siteUrl, String category);

    List<String> getCategories();
}
