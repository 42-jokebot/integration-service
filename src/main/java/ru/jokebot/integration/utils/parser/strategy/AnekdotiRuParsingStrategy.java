package ru.jokebot.integration.utils.parser.strategy;

import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import ru.jokebot.integration.exception.parser.ParsingException;
import ru.jokebot.integration.model.dto.ExternalResourceJokeDto;
import ru.jokebot.integration.utils.parser.AnekdotiRuParsingUtils;
import ru.jokebot.integration.utils.parser.ParsingStrategy;

import java.util.List;

@Slf4j
public class AnekdotiRuParsingStrategy implements ParsingStrategy {

    @Override
    public ExternalResourceJokeDto parse(String siteUrl, String category) {
        Document document;
        try {
            document = Jsoup.connect(siteUrl).get();
            return new ExternalResourceJokeDto(AnekdotiRuParsingUtils.parseJokeText(document), category, "ru");
        } catch (Exception e) {
            throw new ParsingException("Exception while connecting to " + siteUrl + ": " + e.getMessage());
        }
    }

    @Override
    public List<String> getCategories() {
        return List.of("случайный");
    }
}
