package ru.jokebot.integration.utils.parser;

import ru.jokebot.integration.model.dto.ExternalResourceJokeDto;

public record ParseResource(String siteUrl,
                            ParsingStrategy strategy) {

    public ExternalResourceJokeDto parseJoke(String category) {
        return strategy.parse(siteUrl, category);
    }
}
