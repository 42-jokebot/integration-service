package ru.jokebot.integration.utils.bot;

import com.pengrad.telegrambot.model.request.InlineKeyboardButton;
import com.pengrad.telegrambot.model.request.InlineKeyboardMarkup;
import lombok.experimental.UtilityClass;
import org.springframework.context.MessageSource;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Locale;

@UtilityClass
public class TelegramInlineKeyboardFactory {

    /**
     * Метод реализует создание инлайн (прикрепляймой к сообщениям)
     * клавиатуры для реакции на шутку. При нажатии на шутку телеграм боту
     * возвращается callbackData с заранее заготовленными данными like/dislike/nextJoke.
     * @param messageSource инструмент для интернационализации сообщений, требуется
     * для локализации кнопок.
     * @param locale язык пользователя
     * @return {@link com.pengrad.telegrambot.model.request.InlineKeyboardMarkup}
     */
    public static InlineKeyboardMarkup createReactionKeyboard(MessageSource messageSource, String locale) {
        InlineKeyboardMarkup keyboard = new InlineKeyboardMarkup();
        InlineKeyboardButton likeButton = new InlineKeyboardButton().callbackData("like");
        likeButton.setText("\uD83D\uDC4D");
        InlineKeyboardButton dislikeButton = new InlineKeyboardButton().callbackData("dislike");
        dislikeButton.setText("\uD83D\uDC4E");

        String nextJokeButtonText = messageSource.getMessage("nextJokeButton", null, Locale.of(locale));
        InlineKeyboardButton nextJokeButton = new InlineKeyboardButton(nextJokeButtonText).callbackData("nextJoke");
        String toNavButtonText = messageSource.getMessage("toNavButton", null, Locale.of(locale));
        InlineKeyboardButton navButton = new InlineKeyboardButton(toNavButtonText).callbackData("toNav");

        keyboard.addRow(likeButton, dislikeButton);
        keyboard.addRow(nextJokeButton);
        keyboard.addRow(navButton);
        return keyboard;
    }

    /**
     * Метод реализует создание инлайн (прикрепляймой к сообщениям)
     * клавиатуры для навигации по боту. При нажатии на шутку телеграм боту
     * возвращается callbackData с данными по определенной кнопке.<br>
     * Колбэки для обработки:
     * <li> getJoke - случайная шутка</li>
     * <li> getJokeByCategory - поиск шутки по категории</li>
     * <li> getCategories - получение списка всех категорий</li>
     * <li> changeLanguage - смена языка пользователя</li>
     * </ul>
     * @param messageSource инструмент для интернационализации сообщений,
     * необходим для локализации кнопок.
     * @param locale язык пользователя
     * @return {@link com.pengrad.telegrambot.model.request.InlineKeyboardMarkup}
     */
    public static InlineKeyboardMarkup createMenuKeyboard(MessageSource messageSource, String locale) {
        InlineKeyboardMarkup keyboard = new InlineKeyboardMarkup();

        String getJokeButtonText = messageSource.getMessage("getJokeButton", null, Locale.of(locale));
        String getJokeByCategoryButtonText = messageSource.getMessage("getJokeByCategoryButton", null, Locale.of(locale));
        String getCategoriesButtonText = messageSource.getMessage("getCategoriesButton", null, Locale.of(locale));
        String languageChangeButtonText = messageSource.getMessage("changeLanguageButton", null, Locale.of(locale));

        InlineKeyboardButton getJokeButton = new InlineKeyboardButton(getJokeButtonText).callbackData("getJoke");
        InlineKeyboardButton getJokeByCategoryButton = new InlineKeyboardButton(getJokeByCategoryButtonText).callbackData("getJokeByCategory");
        InlineKeyboardButton getCategories = new InlineKeyboardButton(getCategoriesButtonText).callbackData("categoryData");
        InlineKeyboardButton changeLanguage = new InlineKeyboardButton(languageChangeButtonText).callbackData("changeLanguage");

        keyboard.addRow(getJokeButton, getJokeByCategoryButton);
        keyboard.addRow(getCategories);
        keyboard.addRow(changeLanguage);

        return keyboard;
    }

    /**
     * Метод реализует создание инлайн кнопок для сообщения с шуткой
     * после оценивания.
     * @param messageSource инструмент для интернационализации сообщений,
     * необходим для локализации кнопок.
     * @param locale язык пользователя
     * @return {@link com.pengrad.telegrambot.model.request.InlineKeyboardMarkup}
     */
    public static InlineKeyboardMarkup createReactedStateKeyboard(MessageSource messageSource, String locale) {
        InlineKeyboardMarkup keyboard = new InlineKeyboardMarkup();

        String nextJokeButtonText = messageSource.getMessage("nextJokeButton", null, Locale.of(locale));
        InlineKeyboardButton nextJokeButton = new InlineKeyboardButton(nextJokeButtonText).callbackData("nextJoke");
        String toNavButtonText = messageSource.getMessage("toNavButton", null, Locale.of(locale));
        InlineKeyboardButton navButton = new InlineKeyboardButton(toNavButtonText).callbackData("toNav");

        keyboard.addRow(nextJokeButton);
        keyboard.addRow(navButton);
        return keyboard;
    }

    /**
     * Метод реализует создание клавиатуры с набором всех категорий
     * Клавиатура включает в себя множество рядов по 2 кнопки категорий, если
     * категорий нечётное количество - лишняя кнопка выносится в отдельный ряд.
     * @param categories существующие категории шуток в БД
     * @return {@link com.pengrad.telegrambot.model.request.InlineKeyboardMarkup}
     */
    public static InlineKeyboardMarkup createCategoryListKeyboard(List<String> categories) {
        InlineKeyboardMarkup keyboard = new InlineKeyboardMarkup();

        boolean isRowCountOdd = categories.size() % 2 != 0;
        int categoryRowCount = categories.size() / 2;

        for (int i = 0, index = 0; i < categoryRowCount; i += 1, index += 2) {
            String firstCategoryName = categories.get(index);
            String secondCategoryName = categories.get(index + 1);
            keyboard.addRow(
                    new InlineKeyboardButton(StringUtils.capitalize(firstCategoryName)).callbackData(firstCategoryName),
                    new InlineKeyboardButton(StringUtils.capitalize(secondCategoryName)).callbackData(secondCategoryName)
            );
        }

        if (isRowCountOdd) {
            keyboard.addRow(new InlineKeyboardButton(categories.getLast()).callbackData(categories.getLast()));
        }

        InlineKeyboardButton navButton = new InlineKeyboardButton("⏪").callbackData("toNav");
        keyboard.addRow(navButton);

        return keyboard;
    }
}
