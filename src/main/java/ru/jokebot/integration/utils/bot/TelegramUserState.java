package ru.jokebot.integration.utils.bot;

public enum TelegramUserState {
    Menu,
    ChoosingCategory,
    LookingJokeByCategory,
    LookingRandomJokes
}
