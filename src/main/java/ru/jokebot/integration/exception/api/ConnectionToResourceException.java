package ru.jokebot.integration.exception.api;

public class ConnectionToResourceException extends RuntimeException {

    public ConnectionToResourceException(String message) {
        super(message);
    }
}
