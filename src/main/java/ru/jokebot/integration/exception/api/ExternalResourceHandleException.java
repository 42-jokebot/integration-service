package ru.jokebot.integration.exception.api;

public class ExternalResourceHandleException extends RuntimeException {

    public ExternalResourceHandleException(String message) {
        super(message);
    }
}
