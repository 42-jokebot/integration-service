package ru.jokebot.integration.exception.parser;

public class ParsingException extends RuntimeException {

    public ParsingException(String message) {
        super(message);
    }
}
