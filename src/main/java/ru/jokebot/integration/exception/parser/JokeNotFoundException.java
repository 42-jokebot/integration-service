package ru.jokebot.integration.exception.parser;

public class JokeNotFoundException extends RuntimeException {

    public JokeNotFoundException(String message) {
        super(message);
    }
}
