package ru.jokebot.integration.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.jokebot.integration.model.dto.ExternalResourceJokeDto;
import ru.jokebot.integration.service.IntegrationService;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class InternalApiController {

    private final IntegrationService integrationService;

    @GetMapping("/requestJoke")
    public ResponseEntity<ExternalResourceJokeDto> handleRequestJoke(@RequestParam String category,
                                                                     @RequestParam String locale) {
        return ResponseEntity.ok(integrationService.getJokeFromExternalResource(category, locale));
    }
}
