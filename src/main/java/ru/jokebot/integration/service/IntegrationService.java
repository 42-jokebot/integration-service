package ru.jokebot.integration.service;

import ru.jokebot.integration.model.dto.CategoryDto;
import ru.jokebot.integration.model.dto.ExternalResourceJokeDto;

import java.util.List;

public interface IntegrationService {

    // Joke and categories requests from Telegram
    String requestJoke(Long userId, String locale);

    String requestJoke(Long userId, String category, String locale);

    List<CategoryDto> requestCategories(String locale);

    Long requestJokeId(String jokeText);

    // Getting joke from external service
    ExternalResourceJokeDto getJokeFromExternalResource(String category, String locale);

    // Messaging between services
    void sendReactionDataToStatisticService(Long userId, Long jokeId, Integer reaction);

    void sendJokeSendEventToStatisticService(Long userId, Long jokeId);
}
