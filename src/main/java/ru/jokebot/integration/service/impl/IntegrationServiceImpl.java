package ru.jokebot.integration.service.impl;

import feign.FeignException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import ru.jokebot.integration.api.JokeApiClient;
import ru.jokebot.integration.api.JokeServiceClient;
import ru.jokebot.integration.exception.api.ConnectionToResourceException;
import ru.jokebot.integration.exception.api.ExternalResourceHandleException;
import ru.jokebot.integration.exception.parser.JokeNotFoundException;
import ru.jokebot.integration.model.dto.CategoryDto;
import ru.jokebot.integration.model.dto.ExternalResourceJokeDto;
import ru.jokebot.integration.model.dto.JokeSendMessageDto;
import ru.jokebot.integration.model.dto.ReactionMessageDto;
import ru.jokebot.integration.model.dto.response.JokeApiResponse;
import ru.jokebot.integration.model.dto.response.JokeGetResponse;
import ru.jokebot.integration.service.IntegrationService;
import ru.jokebot.integration.utils.parser.ExternalResourceParser;
import ru.jokebot.logging.annotation.Logged;

import java.util.List;

@Service
@Logged
@RequiredArgsConstructor
@Slf4j
public class IntegrationServiceImpl implements IntegrationService {

    // External API Endpoints
    private final JokeApiClient jokeApiClient;

    // Internal API Endpoints
    private final JokeServiceClient jokeServiceClient;

    private final ExternalResourceParser externalResourceParser;

    private final KafkaTemplate<String, Object> kafkaTemplate;

    @Override
    public String requestJoke(Long userId, String locale) {
        try {
            JokeGetResponse response = jokeServiceClient.getJoke(userId, locale);
            sendJokeSendEventToStatisticService(userId, response.id());

            return response.joke();
        } catch (FeignException e) {
            log.warn("Can't get joke from Joke Service: {}", e.getMessage());
            throw new ConnectionToResourceException("Can't connect to Joke Service");
        }
    }

    @Override
    public String requestJoke(Long userId, String category, String locale) {
        try {
            JokeGetResponse response = jokeServiceClient.getJokeByCategory(category, userId);
            sendJokeSendEventToStatisticService(userId, response.id());

            return response.joke();
        } catch (FeignException e) {
            log.warn("Can't get joke from Joke Service: {}", e.getMessage());
            throw new ConnectionToResourceException("Can't connect to Joke Service");
        }
    }

    @Override
    public List<CategoryDto> requestCategories(String locale) {
        try {
            return jokeServiceClient.getCategories(locale).stream()
                    .map(category -> new CategoryDto(category.id(), category.categoryName()))
                    .toList();
        } catch (FeignException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Long requestJokeId(String jokeText) {
        try {
            JokeGetResponse response = jokeServiceClient.getJokeByText(jokeText);
            return response.id();
        } catch (FeignException e) {
            log.error("Can't get joke by service: {}", e.getMessage());

            if (e.status() == 404) {
                throw new JokeNotFoundException("Can't find joke by text");
            } else {
                throw new ConnectionToResourceException("Can't connect to Joke Service");
            }
        }
    }

    @Override
    public ExternalResourceJokeDto getJokeFromExternalResource(String category, String locale) {
        if (locale.equals("ru")) {
            return externalResourceParser.getJoke(category, locale);
        }

        try {
            String jokeType = List.of("Spooky", "Christmas").contains(category) ? "twopart" : "single";
            JokeApiResponse response = jokeApiClient.getRandomJoke(category, jokeType);
            String jokeText = jokeType.equals("single")
                    ? response.joke()
                    : response.setup() + "\n" + response.delivery();

            return new ExternalResourceJokeDto(jokeText, response.category(), locale);
        } catch (FeignException e) {
            throw new ExternalResourceHandleException("Error while getting joke from external service: " + e.getMessage());
        }
    }

    @Override
    public void sendReactionDataToStatisticService(Long userId, Long jokeId, Integer reaction) {
        ReactionMessageDto reactionMessageDto = new ReactionMessageDto(userId, reaction, jokeId);
        kafkaTemplate.send("queue.jokes.reaction", reactionMessageDto);
    }

    @Override
    public void sendJokeSendEventToStatisticService(Long userId, Long jokeId) {
        JokeSendMessageDto jokeSendMessageDto = new JokeSendMessageDto(userId, jokeId);
        kafkaTemplate.send("queue.jokes.give", jokeSendMessageDto);
    }
}
