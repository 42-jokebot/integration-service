package ru.jokebot.integration.service.impl;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.model.CallbackQuery;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.reaction.ReactionTypeEmoji;
import com.pengrad.telegrambot.model.request.InlineKeyboardMarkup;
import com.pengrad.telegrambot.request.AnswerCallbackQuery;
import com.pengrad.telegrambot.request.EditMessageReplyMarkup;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.request.SetMessageReaction;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import ru.jokebot.integration.exception.api.ConnectionToResourceException;
import ru.jokebot.integration.exception.parser.JokeNotFoundException;
import ru.jokebot.integration.model.dto.CategoryDto;
import ru.jokebot.integration.model.entity.User;
import ru.jokebot.integration.repository.UserRepository;
import ru.jokebot.integration.service.IntegrationService;
import ru.jokebot.integration.utils.bot.TelegramInlineKeyboardFactory;
import ru.jokebot.integration.utils.bot.TelegramUserState;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
@SuppressWarnings("deprecation")
public class TelegramBotService {

    private final TelegramBot bot;
    private final UserRepository userRepository;
    private final MessageSource messageSource;
    private final IntegrationService integrationService;
    private final Map<Long, TelegramUserState> userStateCache = new HashMap<>();
    private final Map<Long, String> lastWatchedCategoryByUser = new HashMap<>();

    @PostConstruct
    public void init() {
        bot.setUpdatesListener(
                updates -> {
                    updates.forEach(this::handleBotUpdate);
                    return UpdatesListener.CONFIRMED_UPDATES_ALL;
                }
        );
    }
    private void handleBotUpdate(Update update) {
        if (update.callbackQuery() != null) {
            Long telegramUserId = update.callbackQuery().from().id();
            String locale = update.callbackQuery().from().languageCode();
            User user = authentificateUser(telegramUserId, locale);
            userStateCache.putIfAbsent(telegramUserId, TelegramUserState.Menu);
            handleCallbackQuery(update.callbackQuery(), user);
        }
        if (update.message() != null) {
            Long telegramUserId = update.message().from().id();
            String locale = update.message().from().languageCode();
            User user = authentificateUser(telegramUserId, locale);
            userStateCache.putIfAbsent(telegramUserId, TelegramUserState.Menu);
            handleCommand(update.message(), user);
        }
    }

    // Callback handlers
    private void handleCallbackQuery(CallbackQuery callbackQuery, User user) {
        String callbackData = callbackQuery.data();
        TelegramUserState userState = userStateCache.get(user.getId());

        if (callbackData.equals("nextJoke") && (userState == TelegramUserState.LookingJokeByCategory
                || userState == TelegramUserState.LookingRandomJokes)) {
            handleNextJokeCallback(callbackQuery, user);
            return;
        }

        if (userState == TelegramUserState.ChoosingCategory && !callbackData.equals("toNav")) {
            showCallbackJokeChosenByCategory(callbackQuery, user);
        }

        switch (callbackData) {
            case "toNav" -> handleToNavCallback(callbackQuery, user);
            case "like", "dislike" -> handleReactionCallbacks(callbackQuery, user);
            case "getJoke" -> handleRandomJokeCallback(callbackQuery, user);
            case "getJokeByCategory" -> handleGetJokeByCategoryCallback(callbackQuery, user);
            case "categoryData" -> handleGetCategoriesCallback(callbackQuery, user);
            case "changeLanguage" -> handleLanguageChangeCallback(callbackQuery, user);
            default -> {
                log.error("Caught unexpected callback {}", callbackData);
                bot.execute(new AnswerCallbackQuery(callbackQuery.id()));
            }
        }
    }

    // Обработчик нажатия по кнопке "в меню"/"to navigation".
    private void handleToNavCallback(CallbackQuery callbackQuery, User user) {
        userStateCache.put(user.getId(), TelegramUserState.Menu);
        handleInfoCommand(callbackQuery.message(), user);
        // Hiding last message buttons
        bot.execute(new EditMessageReplyMarkup(callbackQuery.message().chat().id(), callbackQuery.message().messageId())
                .replyMarkup(new InlineKeyboardMarkup()));
    }

    private void handleNextJokeCallback(CallbackQuery callbackQuery, User user) {
        TelegramUserState userState = userStateCache.get(user.getId());

        switch (userState) {
            case LookingJokeByCategory -> {
                String jokeCategory = lastWatchedCategoryByUser.get(user.getId());
                sendJokeByCategory(callbackQuery.message(), user, jokeCategory);
                bot.execute(new AnswerCallbackQuery(callbackQuery.id()));
                // Hiding last message buttons
                bot.execute(new EditMessageReplyMarkup(callbackQuery.message().chat().id(), callbackQuery.message().messageId())
                        .replyMarkup(new InlineKeyboardMarkup()));
            }
            case LookingRandomJokes -> {
                handleGetJokeCommand(callbackQuery.message(), user);
                bot.execute(new AnswerCallbackQuery(callbackQuery.id()));
                // Hiding last message buttons
                bot.execute(new EditMessageReplyMarkup(callbackQuery.message().chat().id(), callbackQuery.message().messageId())
                        .replyMarkup(new InlineKeyboardMarkup()));
            }
            default -> {
                log.error("Caught unexpected state on next joke callback: {}", userState);
                bot.execute(new AnswerCallbackQuery(callbackQuery.id()));
            }
        }
    }

    /**
     * Метод отвечает за создание сообщения c текстом шутки по определённой категории.
     * Не следует путать этот метод с <em>handleGetJokeByCategoryCallback</em>, отвечающий за выбор категории
     * пользователем.
     * @param callbackQuery объект содержащий информацию о сообщении, чате, источнике вызова, ожидается поле callbackData
     * в объекте callbackQuery будет название категории.
     * @param user модель пользователя Telegram
     */
    private void showCallbackJokeChosenByCategory(CallbackQuery callbackQuery, User user) {
        String jokeCategory = callbackQuery.data();
        userStateCache.put(user.getId(), TelegramUserState.LookingJokeByCategory);
        lastWatchedCategoryByUser.put(user.getId(), jokeCategory);
        sendJokeByCategory(callbackQuery.message(), user, jokeCategory);
        // Hiding last message buttons
        bot.execute(new EditMessageReplyMarkup(callbackQuery.message().chat().id(), callbackQuery.message().messageId())
                .replyMarkup(new InlineKeyboardMarkup()));
    }

    private void handleReactionCallbacks(CallbackQuery callbackQuery, User user) {
        String message = callbackQuery.message().text();
        try {
            Long jokeId = integrationService.requestJokeId(message);
            int reaction = (callbackQuery.data().equals("like")) ? 1 : -1;

            integrationService.sendReactionDataToStatisticService(user.getId(), jokeId, reaction);
            log.info("Information about the user's {} reaction \"{}\" " +
                            "on joke {} has been sent to the statistics server",
                    user.getId(), callbackQuery.data(), jokeId);
            // Showing user their reaction on joke message
            bot.execute(new SetMessageReaction(
                    callbackQuery.message().chat().id(),
                    callbackQuery.message().messageId(),
                    new ReactionTypeEmoji((reaction == 1) ? "👍" : "👎")));
            // Acknowledge that callback is handled
            bot.execute(new AnswerCallbackQuery(callbackQuery.id()));
            // Hiding reaction buttons
            bot.execute(new EditMessageReplyMarkup(callbackQuery.message().chat().id(), callbackQuery.message().messageId())
                    .replyMarkup(TelegramInlineKeyboardFactory.createReactedStateKeyboard(messageSource, user.getLocale())));
        } catch (JokeNotFoundException | ConnectionToResourceException e) {
            log.error("Error during processing of the user's reaction: {}", e.getMessage());
            log.error("Skipping reaction handle");
            bot.execute(new AnswerCallbackQuery(callbackQuery.id()).showAlert(true).text("Error during reaction handle"));
        }
    }

    private void handleRandomJokeCallback(CallbackQuery callbackQuery, User user) {
        handleGetJokeCommand(callbackQuery.message(), user);
        bot.execute(new AnswerCallbackQuery(callbackQuery.id()));
        // Hiding last message buttons
        bot.execute(new EditMessageReplyMarkup(callbackQuery.message().chat().id(), callbackQuery.message().messageId())
                .replyMarkup(new InlineKeyboardMarkup()));
    }

    /**
     * Метод отвечает за создание сообщения с списком категорий шуток для пользователя,
     * не стоит путать этот метод с <em>showCallbackJokeChosenByCategory</em>, который отвечает
     * за вывод конкретно шутки.
     * @param callbackQuery объект содержащий информацию о сообщении, чате, нажатой кнопке
     * @param user модель пользователя Telegram
     */
    private void handleGetJokeByCategoryCallback(CallbackQuery callbackQuery, User user) {
        userStateCache.put(user.getId(), TelegramUserState.ChoosingCategory);
        List<String> categories = integrationService.requestCategories(user.getLocale()).stream()
                .map(CategoryDto::categoryName).toList();
        String responseMessageText = messageSource.getMessage("categoryChoose", null, Locale.of(user.getLocale()));
        bot.execute(new SendMessage(callbackQuery.message().chat().id(), responseMessageText)
                .replyMarkup(TelegramInlineKeyboardFactory.createCategoryListKeyboard(categories)));
        bot.execute(new AnswerCallbackQuery(callbackQuery.id()));
        // Hiding last message buttons
        bot.execute(new EditMessageReplyMarkup(callbackQuery.message().chat().id(), callbackQuery.message().messageId())
                .replyMarkup(new InlineKeyboardMarkup()));
    }

    private void handleGetCategoriesCallback(CallbackQuery callbackQuery, User user) {
        userStateCache.put(user.getId(), TelegramUserState.ChoosingCategory);
        handleGetCategoriesCommand(callbackQuery.message(), user);
        bot.execute(new AnswerCallbackQuery(callbackQuery.id()));
        // Hiding last message buttons
        bot.execute(new EditMessageReplyMarkup(callbackQuery.message().chat().id(), callbackQuery.message().messageId())
                .replyMarkup(new InlineKeyboardMarkup()));
    }

    private void handleLanguageChangeCallback(CallbackQuery callbackQuery, User user) {
        String newLocale = (user.getLocale().equals("en")) ? "ru" : "en";
        user.setLocale(newLocale);
        userRepository.edit(user);
        SendMessage request = new SendMessage(callbackQuery.message().chat().id(),
                messageSource.getMessage("languageChangeSuccess",
                        new Object[]{newLocale},
                        Locale.of(newLocale))
        );
        bot.execute(request);
        bot.execute(new AnswerCallbackQuery(callbackQuery.id()));
        // Hiding last message buttons
        bot.execute(new EditMessageReplyMarkup(callbackQuery.message().chat().id(), callbackQuery.message().messageId())
                .replyMarkup(new InlineKeyboardMarkup()));
        handleInfoCommand(callbackQuery.message(), user);
    }

    // Command handlers
    private void handleCommand(Message message, User user) {
        String[] args = message.text().split(" ");
        String command = args[0];

        switch (command) {
            case "/start":
                log.info("User {} with id: {} used '/start' command", message.from().username(), user.getId());
                handleStartCommand(message, user);
                break;
            case "/getJoke":
                log.info("User {} with id: {} requested joke", message.from().username(), user.getId());
                handleGetJokeCommand(message, user);
                break;
            case "/getJokeByCategory":
                // Log about user request is inside handler
                handleGetJokeByCategoryCommand(message, user);
                break;
            case "/getCategories":
                log.info("User {} with id: {} requested categories", message.from().username(), user.getId());
                handleGetCategoriesCommand(message, user);
                break;
            case "/changeLanguage":
                log.info("User {} with id: {} requested language change", message.from().username(), user.getId());
                handleChangeLanguageCommand(message, user);
            default:
                log.info("User {} with id: {} used unknown command or called '/info'", message.from().username(), user.getId());
                handleInfoCommand(message, user);
                break;
        }
    }

    private void handleChangeLanguageCommand(Message message, User user) {
        if (message.text().split(" ").length != 2) {
            String response = messageSource.getMessage("incorrectArgument", null, Locale.of(user.getLocale()));
            SendMessage request = new SendMessage(message.chat().id(), response);
            bot.execute(request);
            return;
        }

        String preferLocale = message.text().split(" ")[1];
        if (preferLocale.equals("ru") || preferLocale.equals("en")) {
            user.setLocale(preferLocale);
            userRepository.edit(user);
            SendMessage request = new SendMessage(message.chat().id(),
                    messageSource.getMessage("languageChangeSuccess",
                            new Object[]{preferLocale},
                            Locale.of(preferLocale))
            );
            bot.execute(request);
        } else {
            SendMessage request = new SendMessage(message.chat().id(),
                    messageSource.getMessage("languageChangeError",
                            null,
                            Locale.of(user.getLocale()))
            );
            bot.execute(request);
        }
    }

    private void handleGetJokeByCategoryCommand(Message message, User user) {
        String[] args = message.text().split(" ");
        if (args.length < 2) {
            String response = messageSource.getMessage("incorrectArgument", null, Locale.of(user.getLocale()));
            SendMessage request = new SendMessage(message.chat().id(), response);
            bot.execute(request);
            return;
        }

        log.info("User {} with id: {} requested joke from category: {}", message.from().username(), user.getId(), args[1]);
        String jokeCategory = args[1];
        sendJokeByCategory(message, user, jokeCategory);
    }

    private void handleGetCategoriesCommand(Message message, User user) {
        try {
            String categories = integrationService.requestCategories(user.getLocale()).stream()
                    .map(CategoryDto::categoryName)
                    .collect(Collectors.joining("\n"));
            String response = messageSource.getMessage("categories", new Object[]{categories}, Locale.of(user.getLocale()));
            SendMessage request = new SendMessage(message.chat().id(), response);
            bot.execute(request);
            log.info("Delivered categories to user {} with id: {}", message.from().username(), user.getId());
        } catch (Exception e) {
            log.warn("Error while getting categories: {}", e.getMessage());
            String response = messageSource.getMessage("categoriesError", null, Locale.of(user.getLocale()));
            SendMessage request = new SendMessage(message.chat().id(), response);
            bot.execute(request);
        }
    }

    public void handleStartCommand(Message message, User user) {
        userStateCache.put(user.getId(), TelegramUserState.Menu);
        String userFirstName = message.from().firstName();
        String response = messageSource.getMessage("welcome",
                new Object[]{userFirstName},
                Locale.of(user.getLocale()));
        SendMessage request = new SendMessage(message.chat().id(), response)
                .replyMarkup(TelegramInlineKeyboardFactory.createMenuKeyboard(messageSource, user.getLocale()));
        bot.execute(request);
    }

    public void handleInfoCommand(Message message, User user) {
        userStateCache.put(user.getId(), TelegramUserState.Menu);
        String response = messageSource.getMessage("help",
                null,
                Locale.of(user.getLocale()));
        SendMessage request = new SendMessage(message.chat().id(), response)
                .replyMarkup(TelegramInlineKeyboardFactory.createMenuKeyboard(messageSource, user.getLocale()));
        bot.execute(request);
    }

    public void handleGetJokeCommand(Message message, User user) {
        userStateCache.put(user.getId(), TelegramUserState.LookingRandomJokes);
        String response;
        try {
            response = integrationService.requestJoke(user.getId(), user.getLocale());
            SendMessage request = new SendMessage(message.chat().id(), response)
                    .replyMarkup(TelegramInlineKeyboardFactory.createReactionKeyboard(messageSource, user.getLocale()));
            bot.execute(request);
            log.info("Successfully delivered joke to user {}:{}", user.getId(), message.from().username());
        } catch (ConnectionToResourceException e) {
            log.warn("Couldn't get joke for user {}:{} - {}", user.getId(), message.from().username(), e.getMessage());
            response = messageSource.getMessage("error",
                    null,
                    Locale.of(user.getLocale()));
            SendMessage request = new SendMessage(message.chat().id(), response);
            bot.execute(request);
        }
    }

    public User authentificateUser(Long id, String locale) {
        Optional<User> optionalUser = userRepository.getUserById(id);

        if (optionalUser.isEmpty()) {
            String userLocale = locale;

            if (userLocale.equals("ru") || userLocale.equals("en")) {
                log.info("Got different locale {} for user: {}, changing to default", userLocale, id);
                userLocale = "ru";
            }

            log.info("Registering a new user: {} - {}", id, userLocale);
            return userRepository.save(id, userLocale);
        }
        return optionalUser.get();
    }

    private void sendJokeByCategory(Message message, User user, String jokeCategory) {
        try {
            userStateCache.put(user.getId(), TelegramUserState.LookingJokeByCategory);
            String response = integrationService.requestJoke(user.getId(), jokeCategory, user.getLocale());
            SendMessage request = new SendMessage(message.chat().id(), response)
                    .replyMarkup(TelegramInlineKeyboardFactory.createReactionKeyboard(messageSource, user.getLocale()));
            bot.execute(request);
            log.info("Delivered joke to user {} with id: {}", message.from().username(), user.getId());
        } catch (ConnectionToResourceException e) {
            log.warn("Error during joke request for user {}:{} : {}", message.from().username(), user.getId(), e.getMessage());
            String response = messageSource.getMessage("error", null, Locale.of(user.getLocale()));
            SendMessage request = new SendMessage(message.chat().id(), response);
            bot.execute(request);
        }
    }
}
