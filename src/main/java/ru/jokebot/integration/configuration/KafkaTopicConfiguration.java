package ru.jokebot.integration.configuration;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaAdmin;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaTopicConfiguration {

    @Bean
    public KafkaAdmin kafkaAdmin(@Value("${spring.kafka.bootstrap-servers}")
                                 String bootstrapServers) {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        return new KafkaAdmin(configs);
    }

    @Bean
    public NewTopic topicSave() {
        return new NewTopic("queue.jokes.save", 3, (short) 1);
    }

    @Bean
    public NewTopic topicGive() {
        return new NewTopic("queue.jokes.give", 3, (short) 1);
    }

    @Bean
    public NewTopic topicReaction() {
        return new NewTopic("queue.jokes.reaction", 3, (short) 1);
    }
}
