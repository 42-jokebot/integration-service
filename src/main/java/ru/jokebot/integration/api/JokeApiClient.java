package ru.jokebot.integration.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import ru.jokebot.integration.model.dto.response.JokeApiResponse;

@FeignClient(name = "JokeApi", url = "https://v2.jokeapi.dev/joke")
public interface JokeApiClient {

    @GetMapping("/{category}")
    JokeApiResponse getRandomJoke(@PathVariable("category") String category,
                                  @RequestParam String type);
}
