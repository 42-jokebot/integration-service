package ru.jokebot.integration.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import ru.jokebot.integration.model.dto.response.CategoryGetResponse;
import ru.jokebot.integration.model.dto.response.JokeGetResponse;

import java.util.List;

@FeignClient(name = "jokes-service")
public interface JokeServiceClient {

    @GetMapping("/api/v1/joke")
    JokeGetResponse getJoke(@RequestParam Long userId,
                            @RequestParam String locale);

    @GetMapping("/api/v1/joke/{category}")
    JokeGetResponse getJokeByCategory(@PathVariable String category,
                                      @RequestParam Long userId);

    @GetMapping("/api/v1/categories")
    List<CategoryGetResponse> getCategories(@RequestParam String locale);

    @GetMapping("/api/v1/joke/search")
    JokeGetResponse getJokeByText(@RequestParam String search);
}
