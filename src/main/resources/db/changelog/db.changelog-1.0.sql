--liquibase formatted sql

--changeset delmark:1
CREATE TABLE IF NOT EXISTS user_data (
    id     BIGINT PRIMARY KEY,
    locale VARCHAR(2)
);