package ru.jokebot.integration.utils.parser;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AnekdotiRuParsingUtilsTest {

    @Mock
    private Document htmlDocument;

    @Mock
    private Element htmlElement;

    @Test
    @DisplayName("Парсинг многострочной шутки")
    void parseMultilineJoke() {
        String rawJoke = "ВНИМАНИЕ<br>АНЕКДОТ!<br>Шутка, анекдота не будет";
        String expectedJoke = "ВНИМАНИЕ\nАНЕКДОТ!\nШутка, анекдота не будет";

        when(htmlDocument.getElementsByAttribute("data-id")).thenReturn(new Elements(htmlElement));
        when(htmlElement.attr("data-id")).thenReturn("1");
        when(htmlDocument.getElementsByAttributeValue("data-id", "1")).thenReturn(new Elements(htmlElement));
        when(htmlElement.getElementsByClass("text")).thenReturn(new Elements(htmlElement));
        when(htmlElement.wholeOwnText()).thenReturn(rawJoke);

        assertEquals(expectedJoke, AnekdotiRuParsingUtils.parseJokeText(htmlDocument));
    }
}