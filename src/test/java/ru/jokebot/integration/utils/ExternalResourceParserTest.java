package ru.jokebot.integration.utils;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.jokebot.integration.exception.parser.JokeNotFoundException;
import ru.jokebot.integration.model.dto.ExternalResourceJokeDto;
import ru.jokebot.integration.utils.parser.ExternalResourceParser;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class ExternalResourceParserTest {

    private final ExternalResourceParser externalResourceParser = new ExternalResourceParser();

    private final List<String> categories = List.of("вовочка", "программист", "новогодний", "политика", "автомобили", "случайный");

    @Test
    @DisplayName("Парсер должен находить шутки по всем возможным категориям")
    void parserShouldFindJokesFromAllCategories() {
        for (String category : categories) {
            ExternalResourceJokeDto jokeDto = externalResourceParser.getJoke(category, "ru");
            assertFalse(jokeDto.jokeText().isBlank());
        }
    }

    @Test
    @DisplayName("Значение из парсера не должно быть пустым")
    void parsedJokeShouldNotBeEmpty() {
        ExternalResourceJokeDto joke = externalResourceParser.getJoke("случайный", "ru");
        assertFalse(joke.jokeText().isBlank());
    }

    @Test
    @DisplayName("Категория шутки не должна изменятся после парсинга")
    void categoryShouldNotBeChanged() {
        ExternalResourceJokeDto joke = externalResourceParser.getJoke("случайный", "ru");
        assertEquals("случайный", joke.category());
    }

    @Test
    @DisplayName("Название категории должно возвращаться в изначальном виде")
    void categoryShouldReturnInPrimaryState() {
        ExternalResourceJokeDto firstJoke = externalResourceParser.getJoke("случайный", "ru");
        ExternalResourceJokeDto secondJoke = externalResourceParser.getJoke("Случайный", "ru");
        assertNotEquals(firstJoke.category(), secondJoke.category());
    }

    @Test
    @DisplayName("Исключение при вызове несуществующей категории: англоязычные ресурсы")
    void exceptionShouldBeThrownForNonexistentCategoryEn() {
        assertThrows(JokeNotFoundException.class, () -> externalResourceParser.getJoke("Категория", "en"));
    }

    @Test
    @DisplayName("Исключение при вызове несуществующей категории: русскоязычные ресурсы")
    void exceptionShouldBeThrownForNonexistentCategoryRu() {
        assertThrows(JokeNotFoundException.class, () -> externalResourceParser.getJoke("Категория", "ru"));
    }
}
