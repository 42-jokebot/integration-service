package ru.jokebot.integration.utils.strategy;

import org.junit.jupiter.api.Test;
import ru.jokebot.integration.model.dto.ExternalResourceJokeDto;
import ru.jokebot.integration.utils.parser.ParsingStrategy;
import ru.jokebot.integration.utils.parser.strategy.AnekdotiRuTaggedParsingStrategy;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class AnekdotiRuTaggedParsingStrategyTest {

    private final ParsingStrategy strategy = new AnekdotiRuTaggedParsingStrategy();

    private final List<String> tags = List.of("вовочка", "программист", "новогодний");

    @Test
    void parsedJokeShouldHaveText() {
        for (String category : tags) {
            String siteUrl = "https://anekdot.ru/tags/";
            ExternalResourceJokeDto joke = strategy.parse(siteUrl, category);
            assertNotNull(joke.jokeText());
            assertFalse(joke.jokeText().isEmpty());
        }
    }
}
