package ru.jokebot.integration.utils.strategy;

import org.junit.jupiter.api.Test;
import ru.jokebot.integration.utils.parser.strategy.AnekdotiRuParsingStrategy;

import static org.junit.jupiter.api.Assertions.assertFalse;

public class AnekdotiRuParsingStrategyTest {

    private final AnekdotiRuParsingStrategy strategy = new AnekdotiRuParsingStrategy();

    @Test
    void getJoke() {
        String SITE_URL = "https://www.anekdot.ru/random/anekdot";
        assertFalse(strategy.parse(SITE_URL, "случайный").jokeText().isEmpty());
    }
}
