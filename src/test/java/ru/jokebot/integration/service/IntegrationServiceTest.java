package ru.jokebot.integration.service;

import feign.FeignException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;
import ru.jokebot.integration.api.JokeApiClient;
import ru.jokebot.integration.api.JokeServiceClient;
import ru.jokebot.integration.exception.api.ConnectionToResourceException;
import ru.jokebot.integration.exception.api.ExternalResourceHandleException;
import ru.jokebot.integration.exception.parser.JokeNotFoundException;
import ru.jokebot.integration.model.dto.CategoryDto;
import ru.jokebot.integration.model.dto.ExternalResourceJokeDto;
import ru.jokebot.integration.model.dto.response.CategoryGetResponse;
import ru.jokebot.integration.model.dto.response.JokeApiResponse;
import ru.jokebot.integration.model.dto.response.JokeGetResponse;
import ru.jokebot.integration.service.impl.IntegrationServiceImpl;
import ru.jokebot.integration.utils.parser.ExternalResourceParser;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class IntegrationServiceTest {

    @Mock
    private JokeApiClient jokeApiClient;

    @Mock
    private JokeServiceClient jokeServiceClient;

    @Mock
    private ExternalResourceParser parser;

    @Mock
    private KafkaTemplate<String, Object> kafkaTemplate;

    @InjectMocks
    private IntegrationServiceImpl integrationService;

    @Test
    void randomJokeRequestFromTelegram() {
        JokeGetResponse response = new JokeGetResponse(5L, "test joke");

        when(jokeServiceClient.getJoke(123456789L, "ru")).thenReturn(response);
        assertEquals("test joke", integrationService.requestJoke(123456789L, "ru"));
    }

    @Test
    void randomJokeRequestFromTelegramWhenJokeServiceNotAvailable() {
        when(jokeServiceClient.getJoke(123456789L, "ru")).thenThrow(FeignException.class);
        assertThrows(ConnectionToResourceException.class, () -> integrationService.requestJoke(123456789L, "ru"));
    }

    @Test
    void jokeWithCategoryRequestFromTelegram() {
        JokeGetResponse response = new JokeGetResponse(3L, "test joke");

        when(jokeServiceClient.getJokeByCategory("any", 123456789L)).thenReturn(response);
        assertEquals("test joke", integrationService.requestJoke(123456789L, "any", "ru"));
    }

    @Test
    void jokeWithCategoryRequestFromTelegramWhenJokeServiceNotAvailable() {
        when(jokeServiceClient.getJokeByCategory("any", 123456789L)).thenThrow(FeignException.class);
        assertThrows(ConnectionToResourceException.class, () -> integrationService.requestJoke(123456789L, "any", "ru"));
    }

    @Test
    void categoriesRequestFromTelegram() {
        String locale = "en";
        List<CategoryGetResponse> returnedResponse = List.of(new CategoryGetResponse(1L, "any", "en"));

        when(jokeServiceClient.getCategories(locale)).thenReturn(returnedResponse);
        assertEquals(List.of(new CategoryDto(1L, "any")), integrationService.requestCategories(locale));
    }

    @Test
    void categoriesRequestFromTelegramWhenServiceNotAvailable() {
        String locale = "en";

        when(jokeServiceClient.getCategories(locale)).thenThrow(FeignException.class);
        assertThrows(RuntimeException.class, () -> integrationService.requestCategories(locale));
    }

    @Test
    void externalJokesFromEnglishResources() {
        String jokeText = "test joke";
        String category = "any";
        String locale = "en";

        ExternalResourceJokeDto expectedResponse = new ExternalResourceJokeDto(jokeText, category, locale);
        JokeApiResponse returnedResponse = new JokeApiResponse(null, category, null, jokeText, null,
                null, null, null, null, null);
        when(jokeApiClient.getRandomJoke(category, "single")).thenReturn(returnedResponse);

        assertEquals(expectedResponse, integrationService.getJokeFromExternalResource(category, locale));
    }

    @Test
    void externalJokesFromRussianResources() {
        String jokeText = "test joke";
        String category = "случайно";
        String locale = "ru";

        ExternalResourceJokeDto returnedResponse = new ExternalResourceJokeDto(jokeText, category, locale);
        when(parser.getJoke(category, locale)).thenReturn(returnedResponse);

        assertEquals(returnedResponse, integrationService.getJokeFromExternalResource(category, locale));
    }

    @Test
    void externalJokesFromRussianResourcesWhenResourcesNotAvailable() {
        String category = "случайно";
        String locale = "ru";

        when(parser.getJoke(category, locale)).thenThrow(JokeNotFoundException.class);
        assertThrows(JokeNotFoundException.class, () -> integrationService.getJokeFromExternalResource(category, locale));
    }

    @Test
    void externalJokesFromEnglishResourcesWhenResourceNotAvailable() {
        String category = "any";
        String locale = "en";


        when(jokeApiClient.getRandomJoke(category, "single")).thenThrow(FeignException.class);
        assertThrows(ExternalResourceHandleException.class, () -> integrationService.getJokeFromExternalResource(category, locale));
    }
}
